# Simple Docker image with Anaconda Python 3, boost, build-essential
ARG BUILDER_BASE_IMAGE=jupyter/scipy-notebook:python-3.9
FROM $BUILDER_BASE_IMAGE

LABEL maintainer="Thomas Purcell <purcell@fhi-berlin.mpg.de>"


USER root

RUN apt-get update --yes \
 && apt-get install --yes --no-install-recommends \
    build-essential \
    g++ \
    cmake\
    liblapack-dev \
    libblas-dev \
    zlib1g-dev \
    libboost-system-dev \
    libboost-filesystem-dev \
    libboost-mpi-dev \
    libboost-serialization-dev \
    libfmt-dev \
    libnlopt-dev \
    libnlopt-cxx-dev \
    coinor-libclp-dev \
    r-base \
 && apt-get clean && rm -rf /var/lib/apt/lists/*


USER ${NB_UID}

RUN mamba install --yes \
    'toml' \
    'pytest' \
    'plotly' \
    'shap' \
    'SALib' \
    'rpy2' \
    'lime' \
 && mamba clean --all -f -y \
 && Rscript -e "install.packages(c('shapr'), repos='https://cran.rstudio.com')" \
 && fix-permissions "${CONDA_DIR}" \
 && fix-permissions "/home/${NB_USER}"


USER root

WORKDIR /opt/sissopp
COPY third_party/sissopp/ .
RUN mkdir build \
 && cd build/ \
 && cmake \
    -DCMAKE_CXX_COMPILER=g++ \
    -DCMAKE_C_COMPILER=gcc \
    -DCMAKE_CXX_FLAGS="-O3" \
    -DBUILD_PYTHON=ON \
    -DBUILD_PARAMS=ON \
    -DBUILD_EXE=OFF \
    -DBUILD_TESTS=OFF \
    ../ \
 && make \
 && make install \
 && make test \
 && cd ../ \
 && rm -rf build/ \
 && fix-permissions "/opt/sissopp"


USER ${NB_UID}
WORKDIR /home/${NB_USER}

COPY --chown=${NB_UID}:${NB_GID} notebook ./notebook
