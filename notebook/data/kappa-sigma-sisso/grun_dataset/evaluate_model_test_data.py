from sissopp.postprocess.utils import load_model
from sissopp.py_interface.import_dataframe import strip_units
from sissopp.postprocess.check_cv_convergence import jackknife_cv_conv_est

import pandas as pd
import numpy as np
import os

def get_data_dct(df):
    dct = {}
    for key in df.columns:
        dct[key] = df[key].to_numpy().astype(float)
    return dct

def get_train_direc_dim():
    test_df = strip_units(f"data_test.csv")
    cv_num = 0

    try:
        mean_val_rmse_1, var_val_rmse_1 = jackknife_cv_conv_est(
            f"p_depth_1/cv*/"
        )
        mean_val_rmse_3, var_val_rmse_3 = jackknife_cv_conv_est(
            f"p_depth_3/cv*/"
        )

        if mean_val_rmse_1[:-1].min() < mean_val_rmse_3[:-1].min():
            dim = mean_val_rmse_1.argmin() + 1
            comp_1_2 = mean_val_rmse_1[:-1].argmin() + 1
            depth = 1
        else:
            dim = mean_val_rmse_3.argmin() + 1
            comp_1_2 = mean_val_rmse_3[:-1].argmin() + 1
            depth = 3

        dim = max(comp_1_2, dim) if comp_1_2 == 2 else dim
        model = load_model(
            f"p_depth_{depth}/train/models/train_dim_{dim}_model_0.dat"
        )
        data_dct = get_data_dct(test_df)
        prop_est = model.eval_many(data_dct)
        print(mean_val_rmse_1, mean_val_rmse_3)
        return np.abs(
            prop_est - test_df["\\log \\kappa_L"]
        )
    except:
        return np.nan

df_test  = strip_units("data_test.csv")
data_dct = get_data_dct(df_test)
prop = df_test["\\log \\kappa_L"]

out_str = ""
for dd in [1, 3]:
     for ii in range(1, 4):
         model = load_model(f"p_depth_{dd}/train/models/train_dim_{ii}_model_0.dat")
         prop_est = model.eval_many(data_dct)
         out_str += f"{np.sqrt(np.mean((prop - prop_est)**2.0))},"

print(out_str[:-1])

