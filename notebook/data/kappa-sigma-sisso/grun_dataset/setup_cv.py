import pandas as pd
import numpy as np
from pathlib import Path

import os
import json

df = pd.read_csv("data.csv", index_col=0)
inds = df.index.tolist().copy()
sisso = json.load(open("sisso.json"))

n_rounds = 3
n_cv = 10

train_direc = Path(f"train/")
train_direc.mkdir(exist_ok=True)
os.system(f"sed 's/DIREC1/{train_direc}/g' submit.sh > {train_direc}/submit.sh")
sisso["data_file"] = str(Path.cwd() / train_direc / "data.csv")
json.dump(sisso, open(f"{train_direc}/sisso.json", 'w'), indent=4)
df.to_csv(f"train/data.csv", index_label="SB_Material")

for ii in range(n_rounds):
    for jj in range(n_cv):
        cv_direc = Path(f"cv_{ii}{jj}/")
        train_df = pd.read_csv(f"{cv_direc}/data.csv", index_col=0)
        test_df = pd.read_csv(f"{cv_direc}/data_test.csv", index_col=0)
        df.loc[train_df.index,:].to_csv(f"{cv_direc}/data.csv", index_label="SB_Material")
        df.loc[test_df.index,:].to_csv(f"{cv_direc}/data_test.csv", index_label="SB_Material")

        os.system(f"sed 's/DIREC1/{cv_direc}/g' submit.sh > {cv_direc}/submit.sh")
        sisso["data_file"] = str(Path.cwd() / cv_direc / "data.csv")
        json.dump(sisso, open(f"{cv_direc}/sisso.json", 'w'), indent=4)

