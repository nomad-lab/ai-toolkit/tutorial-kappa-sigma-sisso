import pandas as pd
import numpy as np
import json

from pathlib import Path
import os

df = pd.read_csv("data.csv", index_col=0)
inds = np.arange(len(df.index))

np.random.shuffle(inds)

sisso = json.load(open("sisso.json", 'r'))

n_cv = 5
for pd in [1, 3]:
    sisso["leave_out_inds"] = []
    pd_direc = Path(f"p_depth_{pd}/")
    pd_direc.mkdir(exist_ok=True)

    os.system(f"sed 's/PDEPTH/pd_{pd}/g' submit.sh > {pd_direc}/submit.sh")
    sisso["max_feat_param_depth"] = pd

    train_direc = pd_direc / "train"
    train_direc.mkdir(exist_ok=True)
    json.dump(sisso, open(str(train_direc / "sisso.json"), 'w'), indent=4)
    os.system(f"sed 's/DIREC2/train/g' {pd_direc}/submit.sh > {train_direc}/submit.sh")

    for ii in range(n_cv):
        cv_direc = pd_direc / f"cv_{ii:02d}"
        cv_direc.mkdir(exist_ok=True)

        os.system(f"sed 's/DIREC2/cv_{ii:02d}/g' {pd_direc}/submit.sh > {cv_direc}/submit.sh")
        sisso_prev = json.load(open(f"{cv_direc}/sisso.json", 'r'))
        sisso["leave_out_inds"] = sisso_prev["leave_out_inds"]
        json.dump(sisso, open(f"{cv_direc}/sisso.json", 'w'), indent=4)

