# tutorial-kappa-sigma-sisso

Notebook describing the results described in _Accelerating Materials-Space Exploration for Thermal Insulators by Mapping Materials Properties via Artificial Intelligence_ (arXiv: 2204.12968 and submitted to _npj Comput. Mater._)

This notebook will be ported onto the NOMAD AI-toolkit once the new server is running; however, in the mean time it can be accessed by running the docker image provided in the container registry. To access it run the following inside a terminal (or equivelant if accessing from a GUI)

```bash
docker run --rm -p 8888:8888 gitlab-registry.mpcdf.mpg.de/nomad-lab/ai-toolkit/tutorial-kappa-sigma-sisso
```

Then open the supplied link into a browser. To install docker please follow the instructions provided [here](https://docs.docker.com/engine/install/).
